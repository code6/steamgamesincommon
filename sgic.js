var _ = require('lodash');
var http = require('http');
var fs = require('fs');


fs.readFile('alias.json', 'utf8', function (err, dataAlias) {
var alias = JSON.parse(dataAlias);

fs.readFile('steam_api_key.txt', 'utf8', function (err, steamAPIKey) {
    if (err) {
        return console.log('Please put your Steam API key into the file steam_api_key.txt (from http://steamcommunity.com/dev)');
    }

    // http://api.steampowered.com/IPlayerService/GetOwnedGames/v0001/?key=ssss&include_appinfo=1&steamid=iiii&format=json
    function getGamesForSteamId(steamId, callback) {

        return http.get({
            host: 'api.steampowered.com',
            path: '/IPlayerService/GetOwnedGames/v0001/?key='+ steamAPIKey +'&include_appinfo=1&steamid='+ steamId +'&format=json'
        }, function(response) {
            // Continuously update stream with data
            var body = '';
            response.on('data', function(d) {
                body += d;
            });
            response.on('end', function() {
                // Data reception is done, do whatever with it!
                var parsed = JSON.parse(body);
                callback(parsed.response.games);
            });
        });
    }

    var argv = require('minimist')(process.argv.slice(2));
    var reduceMinBy = argv.reduceminby || 0; // --reduceminby 1
    var steamIds = argv._; // rest of args, which are the IDs

    if(reduceMinBy > 0){
        console.log('Minimum owning players set to N-'+reduceMinBy+' -> '+(steamIds.length-reduceMinBy));
    }

    if(steamIds.length < 2){
        console.log('Usage: node sgic.js STEAMID1 STEAMID2 STEAMID3 ...');
        console.log('Optionally use parameter --reduceminby 1 to reduce min owning players required by 1, etc.');
        console.log('SteamIDs are integers (see http://steamidfinder.com), but can be put into the alias.json');
        return;
    }

    steamIds = _.map(steamIds, function(steamId){

        if(alias[steamId]){
            return alias[steamId];

        // not in alias, and not a number
        }else if(_.parseInt(steamId) != steamId){ // true for '33', but not '33a', even though both parse as 33, and neither are originally _.isNumber
            throw Error('Alias '+steamId+' not found in alias.json')
        }
        // just a number
        return steamId;
    });

    var gamesForSteamId = {};
    var oneGameListReturned = _.after(steamIds.length, gotAllGames);

    steamIds.forEach(function (steamId, index) {
        console.log('Looking up games for SteamId ' + steamId);

        getGamesForSteamId(steamId, function(games){
            gamesForSteamId[steamId] = games;
            oneGameListReturned();
        })
    });

    function gotAllGames(){
        // list of all games with their appid, name, users (list of steamIds that own it)
        var gamesGrouped = _(gamesForSteamId)
            .map(function(userGames, user){
                return _.map(userGames,function(game) {
                    return {appid: game.appid, gameName: game.name, user: user};
                }
            )})
            .flatten()
            .groupBy('appid')
            .map(function(group) {
                return {
                    name: group[0].gameName,
                    appid: group[0].appid,
                    users: _.pluck(group, 'user')
                };
            })
            .sortBy('name')
            .value();

        // just a list of games everyone own
        var gamesOfEveryone = _(gamesGrouped)
            .filter(function(game){
                return game.users.length >= steamIds.length - reduceMinBy;
            })
            .pluck('name')
            .value();

        console.log('Games in common: '+gamesOfEveryone.length);
        console.log(gamesOfEveryone);
    }

});
});
